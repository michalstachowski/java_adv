Java Advanced
=============

[![Build Status](https://semaphoreci.com/api/v1/mstachowski/java_adv/branches/zad3/shields_badge.svg)](https://semaphoreci.com/mstachowski/java_adv)
[ ![Codeship Status for michalstachowski/java_adv](https://codeship.com/projects/5e4a0900-dcaf-0133-3a62-760a1f8f56cd/status?branch=zad3)](https://codeship.com/projects/144138)

Ćwiczenie zaliczeniowe nr **3**

- **Uczelnia:** Polsko-Japońska Akademia Technik Komputerowych
Oddział w Gdańsku
- **Kierunek:** Informatyka *1* stopień, semestr *4*
- **Student:** Michał Stachowski (*s13383*)

Projekt ten ma za zadanie utworzyć kalkulator płac w oparciu o dane dostarczone przez użytkownika. Jest to kolejne
ćwiczenie dotyczące obsługi wywołań `HttpServletRequest` w aplikacji Webowej.

Serwerem dla tego projektu jest `Jetty` w wersji 9.3

Skompilowany kod można zobaczyć na stronie na mojej [chmurze](http://178.62.27.37:8080/zadanie-3)

### Sprzęt deweloperski

- **CPU:** Intel Core i3 (2Gen; wersja mobilna)
- **RAM:** 8GiB
- **SSD:** Intel 320 - 120 GiB
- **OS:** Microsoft Windows 10 Pro
- **JAVA:** Oracle Java 1.8u60
- **IDE:** Intellij IDEA 2016.1.1

### Instalacja
Projekt wykorzystuje `Maven`a do kompilacji. Budowanie za pomocą polecenia:
> maven prepare-package war:exploded

**Uwaga** dopisanie do komendy `exploded` jest wymagane

### Obecny status
Projekt jest w fazie `RTR` - zakończono implementacje podstawowych założeń projektu. Gotowe do oceny.

### Changelog
- `RTR1`:
    - Poprawiono czytelność kodu oraz importy
    - Dodano filtry:
        - filtrowanie calli nie będących `POST` w API
        - filtrowanie dostępu do strony z wynikami, kiedy nie ma wynikóws
- `beta3`:
    - Dodano częściowe wsparcie dla [Buddy.Works](http://buddy.works)
    - Poprawiono czytelność strony wynikowej
        - Dodano czytelne etykiety umów o pracę
        - Dodano miesiąć jako element tabeli dla umów `COE` oraz `CO`
    - Uproszczono kod
- `beta2`:
    - Prezentacja wyników
        - Wytwarzanie wyników zostało zaimpl
        - Translacja zapytania z formularza webowego do objektów klas, które rozszerzają `CommonAgreement` została dodana
        - Strona prezentująca wyniki została zaimplementowana (`results.jsp`)
    - Usunięcie elementów służących do debugowania
    - Poprawa formularza `first_step.jsp`
       - Przebudowane wykorzystanie flagi `nextStep`
- `beta1`:
    - Kalkulator umowy o pracę (`COE`) został zaimplementowany
    - Kalkulator umowy o dzieło (`CW`) został zaimplementowany
    - Kalkulator umowy zlecenie (`CO`) został zaimplementowany
- `alpha2`:
    - Zakończono rozwój formularza `first_step.jsp`
    - Wywołanie `reset` zostało zaimplementowane
- `alpha1`:
    - Podstawowe pliki projektu zostały dostarczone
    - `Maven` jest skonfigurowany i potrafii wykonywać `code coverage` po stronie `CI`
    - Konfiguracja `CI/CD` jest zakończona
    - Stworzono formularz wspólny dla wszystkich typów umów: `first_step.jsp`
    - Wytworzono klasy dla rożnego rodzaju umów
        - Typy umów są definiowane poprzez enuma `AgreementTypes`
        - Dołączono testy jednostkowe

### TODO
W aktualnej fazie projektu, sekcja `TODO` jest przeznaczona na gromadzenie zadań wymaganych przez projekt:

- [X] Wytworzyć Servlety do obsługi requestów z formularza **P1**
- [X] Wytworzyć algorytmy odpowiedzialne za wyliczanie płac **P0**
- [X] Doimplementować filtrowanie
- [ ] Dodać wsparcie dla generowania plików wyjściowych w formacie `xls` lub `xlsx` **P2**
- [X] Dodać `CI/CD` bazującym na Dockerze: `buddy.works` **P2**
- [X] Poprawić czytelność interfejsu **P2**

package agreements;

import java.time.LocalDate;

/**
 * Created by Michal on 09/04/2016.
 */
public abstract class CommonAgreement {
    AgreementTypes agreementType;
    private int year = LocalDate.now().getYear();
    private boolean isGross;
    private float cashValue;

    CommonAgreement(int year, boolean isGross, float cashValue) {
        setAgreementType();
        this.year = year;
        this.isGross = isGross;
        this.cashValue = cashValue;
    }

    public abstract void setAgreementType();

    public AgreementTypes getAgreementType() {
        return agreementType;
    }

    public int getYear() {
        return year;
    }

    public boolean isGross() {
        return isGross;
    }

    public float getCashValue() {
        return cashValue;
    }
}

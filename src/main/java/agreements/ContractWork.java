package agreements;

/**
 * Created by Michal on 09/04/2016.
 */
public class ContractWork extends NonCOEAgreements {

    public ContractWork(int year, boolean isGross, float cashValue, boolean twentyPercOfRevenue) {
        super(year, isGross, cashValue, twentyPercOfRevenue);
    }

    @Override
    public void setAgreementType() {
        this.agreementType = AgreementTypes.CONTRACT_WORK;
    }
}

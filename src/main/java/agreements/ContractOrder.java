package agreements;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michal on 09/04/2016.
 */
public class ContractOrder extends NonCOEAgreements {

    private boolean pensionContr;
    private boolean lifeAnnuityContr;
    private boolean diseaseContr;
    private boolean healthContr;

    public ContractOrder(int year, boolean isGross, float cashValue, boolean twentyPercOfRevenue,
                         boolean pensionContr, boolean lifeAnnuityContr, boolean diseaseContr, boolean healthContr) {
        super(year, isGross, cashValue, twentyPercOfRevenue);
        this.pensionContr = pensionContr;
        this.lifeAnnuityContr = lifeAnnuityContr;
        this.diseaseContr = diseaseContr;
        this.healthContr = healthContr;
    }

    public Map<String, Boolean> getContrs() {
        Map<String, Boolean> contrs = new HashMap<>();
        contrs.put("pension", this.pensionContr);
        contrs.put("lifeAnnuity", this.lifeAnnuityContr);
        contrs.put("disease", this.diseaseContr);
        contrs.put("health", this.healthContr);
        return contrs;
    }

    @Override
    public void setAgreementType() {
        this.agreementType = AgreementTypes.CONTRACT_ORDER;
    }
}

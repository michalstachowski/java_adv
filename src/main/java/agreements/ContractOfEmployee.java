package agreements;

/**
 * Created by Michal on 09/04/2016.
 */
public class ContractOfEmployee extends CommonAgreement {

    public ContractOfEmployee(int year, boolean isGross, float cashValue) {
        super(year, isGross, cashValue);
    }

    @Override
    public void setAgreementType() {
        this.agreementType = AgreementTypes.CONTRACT_OF_EMPLOYMENT;
    }
}

package agreements;

/**
 * Created by Michal on 09/04/2016.
 */
abstract class NonCOEAgreements extends CommonAgreement {

    private boolean twentyPercentageOfObtainerRevenue;

    NonCOEAgreements(int year, boolean isGross, float cashValue, boolean twentyPercOfRevenue) {
        super(year, isGross, cashValue);
        this.twentyPercentageOfObtainerRevenue = twentyPercOfRevenue;
    }

    public boolean isTwentyPercentageOfObtainerRevenue() {
        return twentyPercentageOfObtainerRevenue;
    }
}

package agreements;

/**
 * Created by Michal on 09/04/2016.
 */
public enum AgreementTypes {
    CONTRACT_OF_EMPLOYMENT("Umowa o pracę"),
    CONTRACT_WORK("Umowa o dzieło"),
    CONTRACT_ORDER("Umowa zlecenie");

    private final String agreementText;

    AgreementTypes(final String agreementText) {
        this.agreementText = agreementText;
    }

    @Override
    public String toString() {
        return this.agreementText;
    }
}

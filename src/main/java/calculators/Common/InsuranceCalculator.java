package calculators.Common;

import java.util.HashMap;
import java.util.Map;

import static java.lang.Math.round;

/**
 * Created by Michal on 18/04/2016.
 */
public class InsuranceCalculator {

    // EMERYTALNE
    private final static float PENSION_FACTOR = 0.0976f;
    // RENTOWE
    private final static float LIFEANNUITY_FACTOR = 0.015f;
    // CHOROBOWE
    private final static float DISEASE_FACTOR = 0.0245f;
    // ZDROWOTNE
    private final static float HEALTH_FACTOR = 0.0775f;
    // NFZ
    private final static float NFZ_FACTOR = 0.09f;

    private float pension;
    private float lifeAnnuity;
    private float disease;
    private float health;
    private float nfzTax;

    private Map<String, Boolean> contrs = new HashMap<>();

    public InsuranceCalculator(float cash, Map<String, Boolean> contrs) {
        this(cash);
        this.contrs = contrs;
    }

    public InsuranceCalculator(float cash) {
        this.pension = cash * PENSION_FACTOR;
        this.lifeAnnuity = cash * LIFEANNUITY_FACTOR;
        this.disease = cash * DISEASE_FACTOR;
        float reducedCash = cash - this.pension - this.lifeAnnuity - this.disease;
        this.nfzTax = reducedCash * NFZ_FACTOR;
        this.health = reducedCash * HEALTH_FACTOR;
    }

    public static float getSumOfPercentages(Map<String, Boolean> contrs) {
        float percentages = 0f;
        if (contrs.get("pension"))
            percentages += PENSION_FACTOR;
        if (contrs.get("lifeAnnuity"))
            percentages += LIFEANNUITY_FACTOR;
        if (contrs.get("disease"))
            percentages += DISEASE_FACTOR;
        return percentages;
    }

    public static float getSumOfPercentages() {
        return PENSION_FACTOR + LIFEANNUITY_FACTOR + DISEASE_FACTOR;
    }

    public static float getNfzFactor() {
        return NFZ_FACTOR;
    }

    public static float getHealthFactor() {
        return HEALTH_FACTOR;
    }

    public float getNfzTax() {
        return nfzTax;
    }

    public float getPension() {
        if (!contrs.containsKey("pension") || contrs.get("pension"))
            return pension;
        else {
            return 0;
        }
    }

    public float getLifeAnnuity() {
        if (!contrs.containsKey("lifeAnnuity") || contrs.get("lifeAnnuity"))
            return lifeAnnuity;
        else {
            return 0;
        }
    }

    public float getDisease() {
        if (!contrs.containsKey("disease") || contrs.get("disease"))
            return disease;
        else {
            return 0;
        }
    }

    public float getHealth() {
        if (!contrs.containsKey("health") || contrs.get("health"))
            return health;
        else {
            return 0;
        }
    }

    public float getInsurenceTotal() {
        return round(getPension() + getDisease() + getLifeAnnuity());
    }

    public Map<String, String> getMapOfResults() {
        Map<String, String> resultsMap = new HashMap<>();
        if (getHealth() > 0)
            resultsMap.put("health", Float.toString(getNfzTax()));
        if (getLifeAnnuity() > 0)
            resultsMap.put("lifeAnnuity", Float.toString(getLifeAnnuity()));
        if (getDisease() > 0)
            resultsMap.put("disease", Float.toString(getDisease()));
        if (getPension() > 0)
            resultsMap.put("pension", Float.toString(getPension()));
        return resultsMap;
    }
}

package calculators.AgreementCalculators;

import agreements.ContractOfEmployee;
import calculators.Common.InsuranceCalculator;

import static java.lang.StrictMath.round;

/**
 * Created by Michal on 19/04/2016.
 */
public class COECalculator extends AgreementsCalculator {

    private final float FREECASH = 556.02f / 12;
    private final float GATHERCOST = 111.25f;
    private InsuranceCalculator insurance;
    private float nfzTax;
    private float totalInsurance;

    public COECalculator(ContractOfEmployee contractOfEmployee) {
        super(contractOfEmployee.getCashValue(), contractOfEmployee.isGross(), contractOfEmployee.getYear());
    }

    @Override
    protected void getFromGross() {
        this.grossCash = this.inputCash;
        calculateInternals();
        this.netCash = this.grossCash - this.totalInsurance - this.nfzTax - this.tax;
    }

    @Override
    protected void getFromNet() {
        this.netCash = this.inputCash;
        float totalPerc = InsuranceCalculator.getSumOfPercentages();
        float nfzPerc = InsuranceCalculator.getNfzFactor();
        float healthPerc = InsuranceCalculator.getHealthFactor();

        float revFact = 1 - totalPerc - nfzPerc - PITTAXPERC + (totalPerc * PITTAXPERC) + healthPerc;
        this.grossCash = (this.netCash - FREECASH - GATHERCOST * PITTAXPERC) / revFact;
        calculateInternals();

    }

    @Override
    protected void getCalcSpecificResults() {
        this.results.putAll(insurance.getMapOfResults());
    }

    @Override
    protected void calculateInternals() {
        this.insurance = new InsuranceCalculator(this.grossCash);
        this.totalInsurance = this.insurance.getInsurenceTotal();
        float reducedCash = this.grossCash - totalInsurance;
        float healthTax = insurance.getHealth();
        this.nfzTax = insurance.getNfzTax();

        this.taxBase = round(reducedCash - GATHERCOST);
        float reqTax = this.taxBase * PITTAXPERC - FREECASH;
        this.tax = round(reqTax - healthTax);
    }
}

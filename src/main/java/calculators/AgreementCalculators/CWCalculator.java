package calculators.AgreementCalculators;

import agreements.ContractWork;

/**
 * Created by Michal on 18/04/2016.
 */
public class CWCalculator extends AgreementsCalculator {

    private float revanuePerc;
    private float revanue;

    public CWCalculator(ContractWork contractWork) {
        super(contractWork.getCashValue(), contractWork.isGross(), contractWork.getYear());
        if (contractWork.isTwentyPercentageOfObtainerRevenue())
            revanuePerc = 0.2f;
        else
            revanuePerc = 0.5f;
    }

    @Override
    protected void getFromGross() {
        this.grossCash = inputCash;
        calculateInternals();
        this.netCash = this.grossCash - this.tax;
    }

    @Override
    protected void calculateInternals() {
        this.revanue = this.revanuePerc * this.grossCash;
        this.taxBase = this.grossCash - this.revanue;
        this.tax = this.taxBase * this.PITTAXPERC;
    }

    @Override
    protected void getFromNet() {
        this.netCash = this.inputCash;
        float revFactor = (1 - (1 - revanuePerc) * PITTAXPERC);
        this.grossCash = this.netCash / revFactor;
        calculateInternals();
    }

    @Override
    protected void getCalcSpecificResults() {
        this.results.put("ravenue", Float.toString(this.revanue));
    }
}

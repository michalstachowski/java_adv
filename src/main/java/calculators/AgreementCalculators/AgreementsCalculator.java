package calculators.AgreementCalculators;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Michal on 18/04/2016.
 */
public abstract class AgreementsCalculator {
    protected final float PITTAXPERC = 0.18f;
    protected boolean isGross;
    protected float grossCash;
    protected float netCash;
    protected float inputCash;
    protected float taxBase;
    protected float tax;
    protected int year;
    protected Map<String, String> results;


    protected AgreementsCalculator(float inputCash, boolean isGross, int year) {
        this.year = year;
        this.inputCash = inputCash;
        this.isGross = isGross;
        this.results = new HashMap<>();
    }

    public Map<String, String> calculate() {
        if (isGross)
            getFromGross();
        else
            getFromNet();
        return getResults();
    }

    private Map<String, String> getResults() {
        results.put("year", Integer.toString(year));
        results.put("net", Float.toString(this.netCash));
        results.put("gross", Float.toString(this.grossCash));
        results.put("tax", Float.toString(this.tax));
        results.put("taxBase", Float.toString(this.taxBase));

        getCalcSpecificResults();
        return this.results;
    }

    protected abstract void getFromGross();

    protected abstract void getFromNet();

    protected abstract void getCalcSpecificResults();

    protected abstract void calculateInternals();

}

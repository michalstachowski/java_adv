package calculators.AgreementCalculators;

import agreements.ContractOrder;
import calculators.Common.InsuranceCalculator;

import java.util.Map;

/**
 * Created by Michal on 18/04/2016.
 */
public class COCalculator extends AgreementsCalculator {

    private InsuranceCalculator insurance;
    private Map<String, Boolean> disabledOptions;
    private float percentageRevenue;
    private float totalInsurence;
    private float taxRevenue;

    public COCalculator(ContractOrder coAgreement) {
        super(coAgreement.getCashValue(), coAgreement.isGross(), coAgreement.getYear());
        if (coAgreement.isTwentyPercentageOfObtainerRevenue())
            this.percentageRevenue = 0.2f;
        else
            this.percentageRevenue = 0.5f;
        this.disabledOptions = coAgreement.getContrs();
    }

    @Override
    protected void getFromGross() {
        this.grossCash = inputCash;
        calculateInternals();
        this.netCash = this.inputCash - this.totalInsurence - this.insurance.getNfzTax() - this.tax;
    }

    @Override
    protected void calculateInternals() {
        this.insurance = new InsuranceCalculator(grossCash, disabledOptions);
        this.totalInsurence = this.insurance.getInsurenceTotal();
        float reducedInput = this.grossCash - this.totalInsurence;
        this.taxRevenue = reducedInput * this.percentageRevenue;
        this.taxBase = reducedInput - this.taxRevenue;
        this.tax = this.taxBase * this.PITTAXPERC - insurance.getHealth();
    }

    @Override
    protected void getCalcSpecificResults() {
        this.results.put("ravenue", Float.toString(taxRevenue));
        this.results.putAll(insurance.getMapOfResults());
    }

    @Override
    protected void getFromNet() {
        this.netCash = this.inputCash;

        float totalInsurancePerc = InsuranceCalculator.getSumOfPercentages(disabledOptions);
        float nfzPercentage = InsuranceCalculator.getNfzFactor();
        float healthPercentage = InsuranceCalculator.getHealthFactor();
        float revFactor = (1 - totalInsurancePerc - nfzPercentage -
                ((1 - this.percentageRevenue) * (1 - totalInsurancePerc) * this.PITTAXPERC - healthPercentage));
        this.grossCash = this.netCash / revFactor;
        calculateInternals();
    }
}

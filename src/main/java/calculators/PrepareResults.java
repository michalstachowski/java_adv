package calculators;

import agreements.*;
import calculators.AgreementCalculators.AgreementsCalculator;
import calculators.AgreementCalculators.COCalculator;
import calculators.AgreementCalculators.COECalculator;
import calculators.AgreementCalculators.CWCalculator;

import java.time.Month;
import java.time.format.TextStyle;
import java.util.*;

/**
 * Created by Michal on 19/04/2016.
 */
public class PrepareResults {

    private List<Map<String, String>> results = new ArrayList<>();
    private AgreementTypes agreementType;

    public PrepareResults(CommonAgreement agreement) {
        this.agreementType = agreement.getAgreementType();

        if (agreementType.equals(AgreementTypes.CONTRACT_WORK))
            prepareCW((ContractWork) agreement);
        else {
            for (int i = 0; i < 12; i++)
                prepareNonCWAgreements(agreement, i);
            calculateTotal();
        }
    }

    private void prepareNonCWAgreements(CommonAgreement agreement, int i) {
        AgreementsCalculator calculator = null;
        if (this.agreementType == AgreementTypes.CONTRACT_OF_EMPLOYMENT)
            calculator = new COECalculator((ContractOfEmployee) agreement);
        else if (this.agreementType == AgreementTypes.CONTRACT_ORDER)
            calculator = new COCalculator((ContractOrder) agreement);
        results.add(addMonthToResults(calculator.calculate(), i));
    }

    private void prepareCW(ContractWork contractWork) {
        CWCalculator cwCalculator = new CWCalculator(contractWork);
        results.add(cwCalculator.calculate());
    }

    private Map<String, String> addMonthToResults(Map<String, String> input, int mIndex) {
        String month = Month.of(mIndex + 1).getDisplayName(TextStyle.FULL, Locale.ENGLISH);
        input.put("month", month);
        return input;
    }

    private void calculateTotal() {
        Map<String, String> total = new HashMap<>();
        Set<String> keys = this.results.get(0).keySet();


        this.results.forEach(result -> {
            keys.forEach(key -> {
                String output = "";
                if (key.equals("month"))
                    output = "-";
                else {
                    float currValue = Float.parseFloat(result.get(key));
                    float currTotal = 0;
                    if (total.get(key) != null)
                        currTotal = Float.parseFloat(total.get(key));
                    output = setTotal(currValue, currTotal, key);
                }
                total.put(key, output);
            });
        });
        this.results.add(total);
    }

    private String setTotal(float currValue, float currTotal, String key) {
        switch (key) {
            case "year":
                return Integer.toString((int) currValue);
            default:
                return Float.toString(currTotal + currValue);
        }
    }

    public List<Map<String, String>> getResults() {
        return results;
    }

    public AgreementTypes contractType() {
        return this.agreementType;
    }
}

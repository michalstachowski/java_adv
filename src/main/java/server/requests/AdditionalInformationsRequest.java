package server.requests;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Michal on 10/04/2016.
 */

@WebServlet("first_step")
public class AdditionalInformationsRequest extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        String year = (String) req.getParameter("year");
        String cash = (String) req.getParameter("cash");
        String isGross = (String) req.getParameter("isGross");
        String agreementCOE = (String) req.getParameter("agreementType[COE]");
        String agreementCW = (String) req.getParameter("agreementType[CW]");
        String agreementCO = (String) req.getParameter("agreementType[CO]");
        String nextPageRediraction = "";

        if (year != null && cash != null && isGross != null &&
                (agreementCO != null || agreementCOE != null || agreementCW != null)) {
            req.setAttribute("nextStep", true);
            if (agreementCOE != null)
                req.setAttribute("COE", true);
            if (agreementCW != null)
                req.setAttribute("CW", true);
            if (agreementCO != null)
                req.setAttribute("CO", true);

            req.setAttribute("year", year);
            req.setAttribute("cash", cash);
            req.setAttribute("isGross", isGross);
            nextPageRediraction = "first_step.jsp";
        } else {
            nextPageRediraction = "reset";
        }

        req.getRequestDispatcher(nextPageRediraction).forward(req, res);

    }

}

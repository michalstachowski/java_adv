package server.requests;

import agreements.ContractOfEmployee;
import agreements.ContractOrder;
import agreements.ContractWork;
import calculators.PrepareResults;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Michal on 18/04/2016.
 */
@WebServlet("calculate")
public class Calculate extends HttpServlet {

    private int year;
    private float cash;
    private boolean isGross;


    private PrepareResults parseToCO(HttpServletRequest req) {
        boolean pensionContr = Boolean.parseBoolean(req.getParameter("pensionContr"));
        boolean percentRev = Boolean.parseBoolean(req.getParameter("percentRevCO"));
        boolean healthContr = Boolean.parseBoolean(req.getParameter("healthContr"));
        boolean diseaseContr = Boolean.parseBoolean(req.getParameter("diseaseContr"));
        boolean lifeAnnuityContr = Boolean.parseBoolean(req.getParameter("lifeAnnuityContr"));

        ContractOrder contractOrder = new ContractOrder(this.year, this.isGross, this.cash, percentRev, pensionContr,
                lifeAnnuityContr, diseaseContr, healthContr);
        return new PrepareResults(contractOrder);
    }

    private PrepareResults parseToCW(HttpServletRequest req) {
        boolean percentRev = Boolean.parseBoolean(req.getParameter("percentRevCW"));
        ContractWork contractWork = new ContractWork(this.year, this.isGross, this.cash, percentRev);
        return new PrepareResults(contractWork);
    }

    private PrepareResults parseToCOE() {
        ContractOfEmployee contractOfEmployee = new ContractOfEmployee(this.year, this.isGross, this.cash);
        return new PrepareResults(contractOfEmployee);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        this.year = Integer.parseInt(req.getParameter("year"));
        this.cash = Float.parseFloat(req.getParameter("cash"));
        this.isGross = Boolean.parseBoolean(req.getParameter("isGross"));

        List<PrepareResults> results = new ArrayList<>();

        if (Boolean.parseBoolean(req.getParameter("agreementType[COE]")))
            results.add(parseToCOE());
        if (Boolean.parseBoolean(req.getParameter("agreementType[CW]")))
            results.add(parseToCW(req));
        if (Boolean.parseBoolean(req.getParameter("agreementType[CO]")))
            results.add(parseToCO(req));

        req.setAttribute("results", results);
        req.getRequestDispatcher("results.jsp").forward(req, res);
    }

}

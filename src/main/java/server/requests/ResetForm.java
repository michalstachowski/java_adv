package server.requests;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Michal on 10/04/2016.
 */
@WebServlet("reset")
public class ResetForm extends HttpServlet {

    private void resetForm(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        if (req.getAttribute("nextStep") != null)
            req.removeAttribute("nextStep");
        req.getRequestDispatcher("index.jsp").forward(req, res);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        resetForm(req, res);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        resetForm(req, res);
    }
}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Michal
  Date: 09/04/2016
  Time: 22:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    Object nextStepObj = request.getAttribute("nextStep");
    Boolean nextStep = false;
    if (nextStepObj != null) {
        nextStep = (boolean) nextStepObj;
    }

    String isDisabled = (nextStep) ? "disabled" : "";
    Object isCO = request.getAttribute("CO");
    Object isCOE = request.getAttribute("COE");
    Object isCW = request.getAttribute("CW");
    Object isGross = request.getAttribute("isGross");
    Object year = request.getAttribute("year");
    Object cash = request.getAttribute("cash");
%>
<html>
<head>
    <title>Krok 1</title>
</head>
<body>
<form method="post" action=<%= (nextStep) ? "\"calculate\"" : "\"first_step\"" %>>
    <table>
        <tr>
            <td>
                <p>
                    Wybierz charakter swojej umowy:
                </p>
                Wybierz rok: <input type="number"
                                    name="year"
                                    min="2010"
                                    step="1"
                                    value=<%= ((year != null) ? year.toString() : "2010")%>
                    <%= isDisabled %>/><br>
                Kwota wynagrodzenia: <input type="number"
                                            name="cash"
                                            min="0"
                                            step="0.01"
                                            value=<%= ((cash != null) ? cash.toString() : "0")%>
                    <%= isDisabled %>/><br>
                <div>
                    <p>
                        Kwota brutto\netto<br>
                        <input type="radio"
                               name="isGross"
                               value="true"
                                <%= (((isGross == null) || (isGross != null && isGross.toString().equals("true"))) ?
                                        "checked" : "") %>
                                <%= isDisabled %>/>Kwota brutto<br>
                        <input type="radio"
                               name="isGross"
                               value="false"
                                <%= (((isGross != null && isGross.toString().equals("false"))) ?
                                        "checked" : "") %>
                                <%= isDisabled %>/>Kwota netto<br>
                    </p>
                </div>
                <div>
                    <p>
                        Wybierz typ umowy<br>
                        <input type="checkbox"
                               name="agreementType[COE]"
                               value="true"
                                <%= ((isCOE != null && isCOE.toString().equals("true")) ? "checked" : "") %>
                                <%= isDisabled %>/>Umowa o prace<br>
                        <input type="checkbox"
                               name="agreementType[CW]"
                               value="true"
                                <%= ((isCW != null && isCW.toString().equals("true")) ? "checked" : "") %>
                                <%= isDisabled %>/>Umowa o dzieło<br>
                        <input type="checkbox"
                               name="agreementType[CO]"
                               value="true"
                                <%= ((isCO != null && isCO.toString().equals("true")) ? "checked" : "") %>
                                <%= isDisabled %>/>Umowa zlecenie<br>
                    </p>
                </div>
            </td>
            <td>
                <div style="display: <%= ((isCW != null) ? "inline" : "none" )%>;">
                    <p>
                        Opcje umowy o dzieło
                    </p>
                    <input type="checkbox" name="percentRevCW" value="true">Koszty uzyskania przychodu<br>
                </div>
                <div style="display: <%= ((isCO != null) ? "inline" : "none" ) %>;">
                    <p>
                        Opcje umowy na zlecenie
                    </p>
                    <input type="checkbox" name="pensionContr" value="true"/>Składka rentowa<br>
                    <input type="checkbox" name="lifeAnnuityContr" value="true"/>Składka emerytalna<br>
                    <input type="checkbox" name="diseaseContr" value="true"/>Składka chorobowa<br>
                    <input type="checkbox" name="healthContr" value="true">Składka zdrowotna<br>
                    <input type="checkbox" name="percentRevCO" value="true">Koszty uzyskania przychodu<br>
                </div>
            </td>
        </tr>
    </table>
    <div>
        <p>
            <%
                if (nextStep) {
            %>
            <input type="hidden" name="year" value="<%= year %>"/>
            <input type="hidden" name="cash" value="<%= cash %>"/>
            <input type="hidden" name="isGross" value="<%= isGross %>"/>
            <input type="hidden" name="agreementType[COE]" value="<%= isCOE %>"/>
            <input type="hidden" name="agreementType[CO]" value="<%= isCO %>"/>
            <input type="hidden" name="agreementType[CW]" value="<%= isCW %>"/>
            <%
                }
            %>

            <input type="button" onclick="location.href='reset'" value="Wstecz"/>
            <input type="submit">
        </p>
    </div>
</form>
</body>
</html>

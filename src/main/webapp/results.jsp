<%@ page import="calculators.PrepareResults" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %><%--
  Created by IntelliJ IDEA.
  User: Michal
  Date: 20/04/2016
  Time: 00:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    List<PrepareResults> list = new ArrayList<>();
    list = (ArrayList<PrepareResults>) request.getAttribute("results");

%>
<html>
<head>
    <title>Results</title>
</head>
<body>
<%
    for (PrepareResults result : list) {
%>
<h1><%= result.contractType().toString() %>
</h1>
<table border="1">
    <tr style="font-weight: bold">
        <%
            for (String colN : result.getResults().get(0).keySet()) {
        %>
        <td><%= colN %>
        </td>
        <%
            }
        %>
    </tr>
    <%
        for (Map<String, String> row : result.getResults()) {
    %>
    <tr>
        <%
            for (String key : row.keySet()) {
        %>
        <td>
            <%
                String valOut;
                if (!(key.equals("year") || key.equals("month"))) {
                    valOut = String.format("%.2f", Float.parseFloat(row.get(key)));
                } else {
                    valOut = row.get(key);
                }
            %>
            <%= valOut %>
        </td>
        <%
            }
        %>
    </tr>
    <%
        }
    %>
</table>
<%
    }
%>
</body>
</html>

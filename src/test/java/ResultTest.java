import agreements.AgreementTypes;
import agreements.ContractOfEmployee;
import agreements.ContractOrder;
import agreements.ContractWork;
import calculators.PrepareResults;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Map;

/**
 * Created by Michal on 19/04/2016.
 */
public class ResultTest {

    private PrepareResults preparedResults;

    @Test
    public void coResultTest() {
        ContractOrder contractOrder = new ContractOrder(2010, false, 10000, true, false, true, false, true);
        preparedResults = new PrepareResults(contractOrder);

        List<Map<String, String>> results = preparedResults.getResults();

        Assert.assertEquals(results.get(0).size(), 9);
        validResults(results);
        Assert.assertEquals(preparedResults.contractType(), AgreementTypes.CONTRACT_ORDER);

    }

    @Test
    public void cwResultTest() {
        ContractWork contractWork = new ContractWork(2010, true, 123, true);
        preparedResults = new PrepareResults(contractWork);

        List<Map<String, String>> results = preparedResults.getResults();

        Assert.assertEquals(results.size(), 1);
        Assert.assertEquals(results.get(0).size(), 6);
        Assert.assertEquals(preparedResults.contractType(), AgreementTypes.CONTRACT_WORK);
    }

    @Test
    public void coeResultTest() {
        ContractOfEmployee contractOfEmployee = new ContractOfEmployee(2010, false, 10000);
        preparedResults = new PrepareResults(contractOfEmployee);

        List<Map<String, String>> results = preparedResults.getResults();

        Assert.assertEquals(results.get(0).size(), 10);
        validResults(results);
        Assert.assertEquals(preparedResults.contractType(), AgreementTypes.CONTRACT_OF_EMPLOYMENT);
    }

    private void validResults(List<Map<String, String>> results) {
        Assert.assertEquals(results.size(), 13);
        Assert.assertEquals(Integer.parseInt(results.get(12).get("year")), 2010);
    }


}

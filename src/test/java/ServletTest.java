import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import server.requests.AdditionalInformationsRequest;
import server.requests.Calculate;
import server.requests.ResetForm;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Michal on 18/04/2016.
 */
public class ServletTest extends Mockito {

    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher rdReset;
    private RequestDispatcher rdFirst;
    private RequestDispatcher rdIndex;
    private RequestDispatcher rdResult;


    @Before
    public void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        rdIndex = mock(RequestDispatcher.class);
        rdReset = mock(RequestDispatcher.class);
        rdFirst = mock(RequestDispatcher.class);
        rdResult = mock(RequestDispatcher.class);

        when(request.getRequestDispatcher("reset")).thenReturn(rdReset);
        when(request.getRequestDispatcher("first_step.jsp")).thenReturn(rdFirst);
        when(request.getRequestDispatcher("index.jsp")).thenReturn(rdIndex);
        when(request.getRequestDispatcher("results.jsp")).thenReturn(rdResult);
    }

    @Test
    public void wrongFulfillRequestTest() throws ServletException, IOException {
        when(request.getParameter("year")).thenReturn("2015");
        when(request.getParameter("cash")).thenReturn("19933.93");
        when(request.getParameter("isGross")).thenReturn("false");

        new AdditionalInformationsRequest().doPost(request, response);
        verify(rdReset).forward(request, response);
    }

    @Test
    public void rightFulfillWCORequestTest() throws ServletException, IOException {
        when(request.getParameter("year")).thenReturn("2015");
        when(request.getParameter("cash")).thenReturn("19933.93");
        when(request.getParameter("isGross")).thenReturn("false");
        when(request.getParameter("agreementType[COE]")).thenReturn("true");
        when(request.getParameter("agreementType[CW]")).thenReturn("true");
        when(request.getParameter("agreementType[CO]")).thenReturn("true");

        new AdditionalInformationsRequest().doPost(request, response);
        verify(rdFirst).forward(request, response);

    }

    @Test
    public void resetTest() throws ServletException, IOException {
        when(request.getAttribute("nextStep")).thenReturn("false");

        new ResetForm().doPost(request, response);
        verify(rdIndex).forward(request, response);

        when(request.getAttribute("nextStep")).thenReturn("true");

        new ResetForm().doGet(request, response);
        verify(rdIndex, times(2)).forward(request, response);
    }

    @Test
    public void calculatePostTest() throws ServletException, IOException {
        when(request.getParameter("year")).thenReturn("2015");
        when(request.getParameter("cash")).thenReturn("19933.93");
        when(request.getParameter("isGross")).thenReturn("false");
        when(request.getParameter("agreementType[COE]")).thenReturn("true");
        when(request.getParameter("agreementType[CW]")).thenReturn("true");
        when(request.getParameter("agreementType[CO]")).thenReturn("true");

        new Calculate().doPost(request, response);

        verify(rdResult).forward(request, response);

    }
}

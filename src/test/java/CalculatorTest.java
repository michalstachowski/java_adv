import agreements.ContractOfEmployee;
import agreements.ContractOrder;
import agreements.ContractWork;
import calculators.AgreementCalculators.COCalculator;
import calculators.AgreementCalculators.COECalculator;
import calculators.AgreementCalculators.CWCalculator;
import calculators.Common.InsuranceCalculator;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Map;

/**
 * Created by Michal on 18/04/2016.
 */
public class CalculatorTest extends Mockito {

    @Test
    public void insuranceCalcTest() {

        InsuranceCalculator calc = new InsuranceCalculator(2000);
        Assert.assertEquals(calc.getPension(), 195.20f, 1);
        Assert.assertEquals(calc.getLifeAnnuity(), 30f, 1);
        Assert.assertEquals(calc.getDisease(), 49f, 1);
        Assert.assertEquals(calc.getNfzTax(), 155f, 1);

        Assert.assertEquals(calc.getInsurenceTotal(), 274, 1);
    }

    @Test
    public void coCalculatorTest() {
        ContractOrder contractOrder = new ContractOrder(2010, true, 2000f, true, true, true, true, true);

        COCalculator coCalculator = new COCalculator(contractOrder);
        Map<String, String> results = coCalculator.calculate();

        verifyDataCO(results);
    }

    @Test
    public void coCalculatorRevAllEnabledTest() {
        ContractOrder contractOrder = new ContractOrder(2010, false, 1455f, true, true, true, true, true);

        COCalculator coCalculator = new COCalculator(contractOrder);
        Map<String, String> results = coCalculator.calculate();

        verifyDataCO(results);
    }

    @Test
    public void coCalculatorRevTest() {
        ContractOrder contractOrderRev = new ContractOrder(2010, false, 1455f, false, false, false, false, false);

        COCalculator coCalculatorRev = new COCalculator(contractOrderRev);
        Map<String, String> resultsRev = coCalculatorRev.calculate();

        Assert.assertEquals(Float.parseFloat(resultsRev.get("taxBase")), 810, 1);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("gross")), 1620, 2);
        Assert.assertEquals(Integer.parseInt(resultsRev.get("year")), 2010);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("tax")), 145, 1);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("ravenue")), 810, 1);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("net")), 1455.0f, 0);
        Assert.assertFalse(resultsRev.containsKey("pension"));
        Assert.assertFalse(resultsRev.containsKey("disease"));
        Assert.assertFalse(resultsRev.containsKey("lifeAnnuity"));
        Assert.assertFalse(resultsRev.containsKey("health"));
    }

    private void verifyDataCO(Map<String, String> results) {
        Assert.assertEquals(Float.parseFloat(results.get("gross")), 2000f, 5);
        Assert.assertEquals(Integer.parseInt(results.get("year")), 2010, 0);
        Assert.assertEquals(Float.parseFloat(results.get("pension")), 195f, 1);
        Assert.assertEquals(Float.parseFloat(results.get("disease")), 49.0f, 1);
        Assert.assertEquals(Float.parseFloat(results.get("lifeAnnuity")), 30f, 1);
        Assert.assertEquals(Float.parseFloat(results.get("health")), 155.32f, 1);
        Assert.assertEquals(Float.parseFloat(results.get("ravenue")), 345f, 1);
        Assert.assertEquals(Float.parseFloat(results.get("taxBase")), 1381f, 2);
        Assert.assertEquals(Float.parseFloat(results.get("tax")), 114f, 1);
        Assert.assertEquals(Float.parseFloat(results.get("net")), 1455f, 5);

    }


    @Test
    public void cwCalculatorTest() {

        ContractWork contractWork = new ContractWork(2011, true, 2000f, true);

        CWCalculator cwCalculator = new CWCalculator(contractWork);
        Map<String, String> results = cwCalculator.calculate();

        Assert.assertEquals(results.get("gross"), Float.toString(2000f));
        Assert.assertEquals(results.get("year"), Integer.toString(2011));
        Assert.assertEquals(results.get("tax"), Float.toString(288f));
        Assert.assertEquals(results.get("ravenue"), Float.toString(400f));
        Assert.assertEquals(results.get("net"), Float.toString(1712f));
        Assert.assertEquals(results.get("taxBase"), Float.toString(1600f));
    }

    @Test
    public void cwCalculatorRevTest() {
        ContractWork contractWorkRev = new ContractWork(2011, false, 1712f, false);
        CWCalculator cwCalculatorRev = new CWCalculator(contractWorkRev);
        Map<String, String> resultsRev = cwCalculatorRev.calculate();
        Assert.assertEquals(Float.parseFloat(resultsRev.get("gross")), 1881f, 1);
        Assert.assertEquals(Integer.parseInt(resultsRev.get("year")), 2011);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("tax")), 169f, 1);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("ravenue")), 940f, 1);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("net")), 1712f, 1);
        Assert.assertEquals(Float.parseFloat(resultsRev.get("taxBase")), 940f, 1);
    }

    @Test
    public void coeCalculatorTest() {
        ContractOfEmployee contractOfEmployee = new ContractOfEmployee(2010, true, 3200);
        COECalculator coeCalculator = new COECalculator(contractOfEmployee);
        Map<String, String> results = coeCalculator.calculate();

        verifyDataCOE(results);
    }

    @Test
    public void coeCalculatorRevTest() {
        ContractOfEmployee contractOfEmployee = new ContractOfEmployee(2010, false, 2295);
        COECalculator coeCalculator = new COECalculator(contractOfEmployee);
        Map<String, String> results = coeCalculator.calculate();

        verifyDataCOE(results);
    }

    private void verifyDataCOE(Map<String, String> results) {
        Assert.assertEquals(Float.parseFloat(results.get("disease")), 78, 1);
        Assert.assertEquals(Float.parseFloat(results.get("gross")), 3200, 10);
        Assert.assertEquals(Integer.parseInt(results.get("year")), 2010, 0);
        Assert.assertEquals(Float.parseFloat(results.get("health")), 248, 2);
        Assert.assertEquals(Float.parseFloat(results.get("pension")), 312, 2);
        Assert.assertEquals(Float.parseFloat(results.get("tax")), 217, 2);
        Assert.assertEquals(Float.parseFloat(results.get("lifeAnnuity")), 48, 2);
        Assert.assertEquals(Float.parseFloat(results.get("taxBase")), 2650, 10);
        Assert.assertEquals(Float.parseFloat(results.get("net")), 2295, 2);
    }

}

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import server.filters.EmptyResultFilter;
import server.filters.GetFilter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Created by Michal on 20/04/2016.
 */
public class FilterTest extends Mockito {

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private HttpServletRequest request;
    private HttpServletResponse response;
    private FilterChain filter;
    private RequestDispatcher rdReset;
    private String initString = "This filter doesn't require configuration";
    private String destroyString = "This filter doesn't require destroy feature";
    private FilterConfig filterConfig;

    @Before
    public void setUp() {
        request = mock(HttpServletRequest.class);
        response = mock(HttpServletResponse.class);
        rdReset = mock(RequestDispatcher.class);
        filter = mock(FilterChain.class);
        filterConfig = mock(FilterConfig.class);

        when(request.getRequestDispatcher("reset")).thenReturn(rdReset);

        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void initiationResultTest() throws ServletException {


        EmptyResultFilter emptyResultFilter = new EmptyResultFilter();
        emptyResultFilter.init(filterConfig);

        Assert.assertEquals(initString, outContent.toString());

    }

    @Test
    public void initiationGetTest() throws ServletException {

        GetFilter getFilter = new GetFilter();
        getFilter.init(filterConfig);

        Assert.assertEquals(initString, outContent.toString());

    }

    @Test
    public void destroyResultTest() throws ServletException {


        EmptyResultFilter emptyResultFilter = new EmptyResultFilter();
        emptyResultFilter.destroy();

        Assert.assertEquals(destroyString, outContent.toString());

    }

    @Test
    public void destroyGetTest() throws ServletException {

        GetFilter getFilter = new GetFilter();
        getFilter.destroy();

        Assert.assertEquals(destroyString, outContent.toString());

    }

    @Test
    public void blockAccessToGetTest() throws ServletException, IOException {
        GetFilter getFilter = new GetFilter();
        when(request.getMethod()).thenReturn("GET");
        getFilter.doFilter(request, response, filter);
        verify(response).sendRedirect("reset");

        when(request.getMethod()).thenReturn("POST");
        getFilter.doFilter(request, response, filter);
        verify(filter).doFilter(request, response);
    }

    @Test
    public void blockEmptyResultTest() throws ServletException, IOException {
        EmptyResultFilter emptyResultFilter = new EmptyResultFilter();
        when(request.getAttribute("results")).thenReturn(null);
        emptyResultFilter.doFilter(request, response, filter);
        verify(response).sendRedirect("reset");

        when(request.getAttribute("results")).thenReturn(mock(Object.class));
        emptyResultFilter.doFilter(request, response, filter);
        verify(filter).doFilter(request, response);
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
        System.setErr(null);
    }

}

import agreements.AgreementTypes;
import agreements.ContractOfEmployee;
import agreements.ContractOrder;
import agreements.ContractWork;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * Created by Michal on 09/04/2016.
 */
public class AgreementTest extends Mockito {

    private int year = 2016;
    private boolean isGross = true;
    private float cashValue = 1234567.89f;

    private boolean isTwentyPercentRevanue = false;


    @Test
    public void contractOfEmployeeTest() {
        ContractOfEmployee contractOfEmployee;

        contractOfEmployee = new ContractOfEmployee(year, isGross, cashValue);

        Assert.assertEquals(year, contractOfEmployee.getYear());
        Assert.assertTrue(contractOfEmployee.isGross());
        Assert.assertEquals(AgreementTypes.CONTRACT_OF_EMPLOYMENT, contractOfEmployee.getAgreementType());
        Assert.assertEquals(cashValue, contractOfEmployee.getCashValue(), 0);
    }

    @Test
    public void contractOfWorkTest() {
        ContractWork contractWork;

        contractWork = new ContractWork(year, isGross, cashValue, isTwentyPercentRevanue);

        Assert.assertEquals(year, contractWork.getYear());
        Assert.assertTrue(contractWork.isGross());
        Assert.assertEquals(AgreementTypes.CONTRACT_WORK, contractWork.getAgreementType());
        Assert.assertEquals(cashValue, contractWork.getCashValue(), 0);
        Assert.assertFalse(contractWork.isTwentyPercentageOfObtainerRevenue());

    }

    @Test
    public void contractOrderTest() {
        boolean pensionContr = true;
        boolean lifeAnnuityContr = false;
        boolean diseaseContr = false;
        boolean healthContr = false;

        ContractOrder contractOrder;
        contractOrder = new ContractOrder(year, isGross, cashValue, isTwentyPercentRevanue, pensionContr,
                lifeAnnuityContr, diseaseContr, healthContr);

        Assert.assertEquals(year, contractOrder.getYear());
        Assert.assertTrue(contractOrder.isGross());
        Assert.assertEquals(cashValue, contractOrder.getCashValue(), 0);
        Assert.assertEquals(AgreementTypes.CONTRACT_ORDER, contractOrder.getAgreementType());
        Assert.assertFalse(contractOrder.isTwentyPercentageOfObtainerRevenue());
        Assert.assertTrue(contractOrder.getContrs().get("pension"));
        Assert.assertFalse(contractOrder.getContrs().get("lifeAnnuity"));
        Assert.assertFalse(contractOrder.getContrs().get("disease"));
        Assert.assertFalse(contractOrder.getContrs().get("health"));
    }

    @Test
    public void agreementTypesTest() {
        AgreementTypes[] possibleTypes = AgreementTypes.values();

        AgreementTypes[] expectedPossibleTypes = new AgreementTypes[]
                {
                        AgreementTypes.CONTRACT_OF_EMPLOYMENT,
                        AgreementTypes.CONTRACT_WORK,
                        AgreementTypes.CONTRACT_ORDER
                };

        Assert.assertArrayEquals(expectedPossibleTypes, possibleTypes);
    }

    @Test
    public void agreementTypesStringTest() {
        AgreementTypes agreementTypeCO = AgreementTypes.CONTRACT_ORDER;
        AgreementTypes agreementTypeCOE = AgreementTypes.CONTRACT_OF_EMPLOYMENT;
        AgreementTypes agreementTypeCW = AgreementTypes.CONTRACT_WORK;

        Assert.assertEquals(agreementTypeCO.toString(), "Umowa zlecenie");
        Assert.assertEquals(agreementTypeCOE.toString(), "Umowa o pracę");
        Assert.assertEquals(agreementTypeCW.toString(), "Umowa o dzieło");
    }
}
